defusedxml==0.4.1
Django==1.8.4
dm.xmlsec.binding==1.3.2
isodate==0.5.0
lxml==3.4.4
M2Crypto==0.22.3
python-saml==2.1.4
